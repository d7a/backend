const chatUser = require('./chat-user/chat-user.service.js');
const conversation = require('./conversation/conversation.service.js');
const message = require('./message/message.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(chatUser);
  app.configure(conversation);
  app.configure(message);
};
