

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [
        /*
        (context) => {
      context.service.emit('change', { status: 'changed' });
    } */ ],
    find: [],
    get: [],
    create: [(context) => {
      context.service.emit('create', { status: 'completed' });
    }],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
