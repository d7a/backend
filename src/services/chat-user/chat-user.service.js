// Initializes the `chat-user` service on path `/chat-user`
const createService = require('feathers-memory');
const hooks = require('./chat-user.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/chat-user', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('chat-user');

  service.hooks(hooks);
};
