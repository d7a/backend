const assert = require('assert');
const app = require('../../src/app');

describe('\'chat-user\' service', () => {
  it('registered the service', () => {
    const service = app.service('chat-user');

    assert.ok(service, 'Registered the service');
  });
});
